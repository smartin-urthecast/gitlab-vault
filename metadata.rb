name             'gitlab-vault'
maintainer       'GitLab Inc.'
maintainer_email 'jeroen@gitlab.com'
license          'MIT'
description      'Library for working with vault secrets'
long_description 'Replace arbitrary node attributes with values from a Chef Vault item'
version          '0.2.0'

depends 'chef-vault'
